const jwt = require('jsonwebtoken');
const { apiSecret } = require('../src/config');

const token = jwt.sign(
	{
		id: 1,
		verified: true
	},
	apiSecret
);

console.log('Token:', token);
