const { RentHistory } = require('../models');

class RentHistoryController {
	async getAll(ctx) {
		const { user } = ctx;

		const allRentHistory = await RentHistory.findAll({
			where: {
				user_id: user.id
			}
		});

		if (!allRentHistory || allRentHistory.length === 0) {
			ctx.throw(404);
		}

		ctx.body = allRentHistory;
	}

	async getOne(ctx) {
		const { id } = ctx.params;
		const { user } = ctx;

		const rentHistory = await RentHistory.findByPk(id, {
			where: {
				user_id: user.id
			}
		});

		if (!rentHistory) {
			ctx.throw(404);
		}

		ctx.body = rentHistory;
	}
}

module.exports = new RentHistoryController();
