const { User, RentHistory } = require('../models');
const jwtHelper = require('../helpers/jwt');
const { validateUserInput } = require('../helpers/validators');

class UsersController {
	async signup(ctx) {
		const { name, email, password } = validateUserInput(ctx, true);

		// TODO: hash the password
		await User.create({ name, email, password });
		const { id, verified } = await User.findOne({ where: { email } });

		const token = await jwtHelper.generateToken({ id, verified });

		ctx.body = {
			user: {
				email,
				name,
				verified
			},
			token
		};
	}

	async login(ctx) {
		const { email, password } = validateUserInput(ctx, false);

		const user = await User.findOne({ where: { email } });

		if (!user) {
			ctx.throw(401);
		}

		if (password !== user.password) {
			ctx.throw(401);
		}

		const { id, name, verified } = user;

		const token = await jwtHelper.generateToken({ id, verified });

		ctx.body = {
			user: {
				email,
				name,
				verified
			},
			token
		};
	}

	async getSelf(ctx) {
		const { user } = ctx;

		const userData = await User.findByPk(user.id, {
			attributes: ['name', 'email', 'verified']
		});

		if (!userData) {
			ctx.throw(404);
		}

		ctx.body = {
			user: userData
		};
	}
}

module.exports = new UsersController();
