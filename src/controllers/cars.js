const moment = require('moment');
const { Car, RentHistory } = require('../models');
const { validateRentInput } = require('../helpers/validators');

class CarsController {
	async getAll(ctx) {
		ctx.body = await Car.findAll();
	}

	async getOne(ctx) {
		const { id } = ctx.params;

		const car = await Car.findByPk(id);

		if (!car) {
			ctx.throw(404);
		}

		ctx.body = car;
	}

	async rent(ctx) {
		const { id } = ctx.params;
		const { id: userId } = ctx.user;

		const {
			startDate,
			endDate,
			paymentMethod,
			paymentDetails
		} = validateRentInput(ctx);

		const car = await Car.findByPk(id);

		if (!car) {
			ctx.throw(404);
		}

		const days = moment(endDate).diff(moment(startDate), 'days');
		const pricePerDay = car.price;
		const priceTotal = pricePerDay * days;

		await RentHistory.create({
			carId: id,
			userId,
			priceTotal,
			pricePerDay,
			startDate,
			endDate,
			paymentMethod,
			paymentDetails
		});

		ctx.status = 200;
	}
}

module.exports = new CarsController();
