const path = require('path');

module.exports = {
	port: process.env.PORT,
	apiSecret: process.env.API_SECRET || 'supersecret',
	databaseConfig: {
		host: process.env.MYSQL_HOST,
		port: process.env.MYSQL_PORT,
		user: 'root',
		password: process.env.MYSQL_ROOT_PASSWORD,
		database: process.env.MYSQL_DATABASE,
		migrations: path.resolve(__dirname, 'src/migrations')
	}
};
