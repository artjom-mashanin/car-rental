const Joi = require('@hapi/joi');

const userInput = isSignup =>
	Joi.object({
		email: Joi.string()
			.email()
			.required(),
		password: Joi.string().required(),
		name: isSignup && Joi.string().required()
	});

const rentCarInput = Joi.object({
	startDate: Joi.date().required(),
	endDate: Joi.date().required(),
	paymentMethod: Joi.string()
		.valid('card', 'bank-transfer')
		.required(),
	paymentDetails: Joi.when('paymentMethod', {
		is: 'card',
		then: Joi.string().required()
	})
});

function validateUserInput(ctx, isSignup) {
	const result = userInput(isSignup).validate(ctx.request.body);

	if (result.error) {
		ctx.throw(400);
	}

	return result.value;
}

function validateRentInput(ctx) {
	const result = rentCarInput.validate(ctx.request.body);

	if (result.error) {
		ctx.throw(400);
	}

	return result.value;
}

module.exports = {
	validateUserInput,
	validateRentInput
};
