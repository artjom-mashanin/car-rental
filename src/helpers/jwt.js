const jwt = require('jsonwebtoken');
const { apiSecret } = require('../config');

async function generateToken(data) {
	const token = await jwt.sign(data, apiSecret);

	return token;
}

module.exports = {
	generateToken
};
