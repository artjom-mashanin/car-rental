const Router = require('koa-router');
const RentHistory = require('../controllers/rent-history');
const jwtVerifier = require('../middleware/jwt-verifier');

const router = new Router({
	prefix: '/rent-history'
});

router.use(jwtVerifier);

router.get('/', RentHistory.getAll);
router.get('/:id', RentHistory.getOne);

module.exports = router;
