const Router = require('koa-router');
const users = require('./users');
const cars = require('./cars');
const rentHistory = require('./rent-history');

const router = new Router();

router.use(users.routes());
router.use(cars.routes());
router.use(rentHistory.routes());

module.exports = router;
