const Router = require('koa-router');
const Users = require('../controllers/users');
const jwtVerifier = require('../middleware/jwt-verifier');

const router = new Router({
	prefix: '/users'
});

router.post('/signup', Users.signup);
router.post('/login', Users.login);
router.get('/self', jwtVerifier, Users.getSelf);

module.exports = router;
