const Router = require('koa-router');
const Cars = require('../controllers/cars');
const jwtVerifier = require('../middleware/jwt-verifier');

const router = new Router({
	prefix: '/cars'
});

router.get('/', Cars.getAll);
router.get('/:id', Cars.getOne);
router.post('/:id/rent', jwtVerifier, Cars.rent);

module.exports = router;
