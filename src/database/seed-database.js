const { Car, User, RentHistory } = require('../models');
const carSeed = require('./seeds/cars');
const userSeed = require('./seeds/users');
const rentalsSeed = require('./seeds/rentals');

async function seedDb() {
	if (!(await Car.count())) {
		await Car.bulkCreate(carSeed);
	}

	if (!(await User.count())) {
		await User.bulkCreate(userSeed);
	}

	if (!(await RentHistory.count())) {
		await RentHistory.bulkCreate(rentalsSeed);
	}
}

module.exports = seedDb;
