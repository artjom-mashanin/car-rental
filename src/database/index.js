const Sequelize = require('sequelize');
const { databaseConfig } = require('../config');

const { host, port, database, user, password } = databaseConfig;

module.exports = new Sequelize(database, user, password, {
	dialect: 'mysql',
	host,
	port,

	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000
	}
});
