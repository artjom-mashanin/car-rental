const moment = require('moment');

module.exports = [
	{
		userId: 1,
		carId: 2,
		pricePerDay: 30,
		priceTotal: 150,
		startDate: moment().format(),
		endDate: moment()
			.add(5, 'days')
			.format(),
		paymentMethod: 'bank-transfer',
		paymentDetails: 'Transfer NR'
	},
	{
		userId: 1,
		carId: 1,
		pricePerDay: 443,
		priceTotal: 2323,
		startDate: moment().format(),
		endDate: moment()
			.add(12, 'days')
			.format(),
		paymentMethod: 'card',
		paymentDetails: 'Some card number'
	}
];
