const jwt = require('jsonwebtoken');

const { apiSecret } = require('../config');

module.exports = (ctx, next) => {
	const { authorization } = ctx.request.header;

	if (!authorization) {
		ctx.throw(401);
	}

	const [type, token] = authorization.split(' ');

	if (type !== 'Bearer') {
		ctx.throw(401);
	}

	try {
		const decodedToken = jwt.verify(token, apiSecret);

		ctx.user = decodedToken;
	} catch (err) {
		console.error(err);

		ctx.throw(401);
	}

	return next();
};
