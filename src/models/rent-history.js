const Sequelize = require('sequelize');
const db = require('../database');

const RentHistory = db =>
	db.define(
		'rent_history',
		{
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			priceTotal: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			pricePerDay: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			startDate: {
				type: Sequelize.DATE,
				allowNull: false
			},
			endDate: {
				type: Sequelize.DATE,
				allowNull: false
			},
			paymentMethod: {
				type: Sequelize.ENUM(['card', 'bank-transfer']),
				allowNull: false
			},
			paymentDetails: {
				type: Sequelize.STRING,
				allowNull: false
			}
		},
		{
			timestamps: true,
			updatedAt: 'updated_at',
			createdAt: 'created_at',
			underscored: true
		}
	);

module.exports = RentHistory;
