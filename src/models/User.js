const Sequelize = require('sequelize');
const db = require('../database');
const RentHistory = require('./rent-history');

const User = db =>
	db.define(
		'user',
		{
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			email: {
				type: Sequelize.STRING,
				unique: true,
				isEmail: true,
				allowNull: false
			},
			password: {
				type: Sequelize.STRING,
				allowNull: false
			},
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			verified: {
				type: Sequelize.BOOLEAN,
				defaultValue: false
			}
		},
		{
			timestamps: true,
			updatedAt: 'updated_at',
			createdAt: 'created_at',
			firstName: 'first_name',
			lastName: 'last_name'
		}
	);

module.exports = User;
