const CarModel = require('./car');
const UserModel = require('./user');
const RentHistoryModel = require('./rent-history');
const database = require('../database');

const Car = CarModel(database);
const User = UserModel(database);
const RentHistory = RentHistoryModel(database);

// Car.hasMany(RentHistory);
// User.hasMany(RentHistory);

RentHistory.belongsTo(Car);
RentHistory.belongsTo(User);

// database.sync({ force: true });

module.exports = { Car, User, RentHistory };
