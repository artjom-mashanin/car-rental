const Sequelize = require('sequelize');

const Car = db =>
	db.define(
		'car',
		{
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			// Maybe name is reduntant? as we can build it frmo model and brand
			name: {
				type: Sequelize.STRING,
				allowNull: false
			},
			type: {
				type: Sequelize.ENUM(
					'sedan',
					'convertible',
					'wagon',
					'truck',
					'motorcycle',
					'bus'
				)
			},
			brand: {
				type: Sequelize.STRING,
				allowNull: false
			},
			model: {
				type: Sequelize.STRING,
				allowNull: false
			},
			year: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			price: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			description: Sequelize.STRING,
			image: Sequelize.STRING,
			active: {
				type: Sequelize.BOOLEAN,
				allowNull: false,
				defaultValue: false
			}
		},
		{
			timestamps: true,
			underscored: true,
			updatedAt: 'updated_at',
			createdAt: 'created_at'
		}
	);

module.exports = Car;
