const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const morgan = require('koa-morgan');
const cors = require('@koa/cors');
const db = require('./database');
const routes = require('./routes');

const seedDb = require('./database/seed-database');

const { port } = require('./config');

async function initDb() {
	try {
		await db.authenticate();
		await db.sync();

		console.log('Database connection succeeded');
	} catch (err) {
		console.error('Error creating DB connecton:', err);
	}
}

async function initServer() {
	const app = new Koa();

	app.use(morgan('common'));
	app.use(bodyParser());
	app.use(cors());

	app.use(routes.routes());
	app.use(routes.allowedMethods());

	app.listen(port, () => {
		console.log(`Server listening on port ${port}`);
	});
}

async function start() {
	await initDb();

	if (process.env.NODE_ENV == 'dev') {
		await seedDb();
	}

	await initServer();
}

module.exports = {
	start
};
