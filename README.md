# API for car rental

## Service outline

- Keep in mind that this is still WIP

## How to start the service?

Run `docker-compose up`. It will spin up the MYSQL db, seed the database and run the server

If you don't want to register a new user - you can use existing one. Email: test@test.com ; Password: test

If you even don't want to login - you can run `npm run generate-token` which will generate a valid JWT token and print it to the console

The server will be exposed to the host on port `4343` and debugger is running on port `44343`

## Routes

### Auth

- POST **/signup/** - signup, DUH
- POST **/login/** - login
- GET **/self/** - Get user data (JWT required)

### Cars

- GET **/cars/** - available cars list
- GET **/cars/:id** - car details
- POST **/cars/:id/rent** - rent a vehicle (JWT required)

### Rent history

- GET **/rent-history/** - get all rent history for user (JWT required)
- GET **/rent-history/:id** - get specific rent details (JWT required)
