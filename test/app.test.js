const { someTest } = require('../src/app');

describe('app.js', () => {
	it('returns 5', () => {
		expect(someTest()).toBe(5);
	});
});
